﻿using System.IO;
using System.Threading.Tasks;

namespace member_manager.Commons
{
    public static class AttachedFileAccess
    {
        const string FILE_STORAGE_DIR = "files";


        static AttachedFileAccess()
        {
            if (!Directory.Exists(FILE_STORAGE_DIR))
            {
                Directory.CreateDirectory(FILE_STORAGE_DIR);
            }
        }

        public static Stream GetFileStream(string fileId, FileMode mode)
        {
            return File.Open(Path.Combine(FILE_STORAGE_DIR, fileId), mode);
        }

        public static async Task CopyFileInto(string sourcePath, string fileId)
        {
            using (Stream input = File.Open(sourcePath, FileMode.Open))
            using (Stream output = File.Open(Path.Combine(FILE_STORAGE_DIR, fileId), FileMode.Create))
            {
                await input.CopyToAsync(output);
            }
        }

        public static void DeleteFile(string fileId)
        {
            File.Delete(Path.Combine(FILE_STORAGE_DIR, fileId));
        }
    }
}
