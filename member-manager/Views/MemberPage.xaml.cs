﻿using member_manager.Commons;
using member_manager.Models;
using member_manager.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace member_manager.Views
{
    /// <summary>
    /// Interaction logic for MemberPage.xaml
    /// </summary>
    public partial class MemberPage : Page
    {
        public delegate void MembersChangedHandler();
        public event MembersChangedHandler MembersChanged;

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<MemberNoteViewModel> m_notes;
        ObservableCollection<AttachedFileViewModel> m_attachedFiles;
        User m_currentUser;
        bool m_createNew;
        Member m_member;
        string m_photoOriginalPathTemp;


        public MemberPage(MainWindow mainwindow, SQLiteConnection dbConn, User currentUser)
        {
            async void LoadComboboxesContents()
            {
                pringLoading.IsActive = true;

                try
                {
                    // Loading cmbGender.
                    cmbGender.Items.Add("MALE");
                    cmbGender.Items.Add("FEMALE");

                    // Loading cmbEyeColor.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM eye_colors";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbEyeColor.Items.Add(reader.GetString(0));
                            }
                        }
                    }

                    // Loading cmbAddressRegion.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM regions";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbAddressRegion.Items.Add(reader.GetString(0));
                            }
                        }
                    }

                    // Loading cmbAddressProvince.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM provinces";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbAddressProvince.Items.Add(reader.GetString(0));
                            }
                        }
                    }

                    // Loading cmbMemberType.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM member_types";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbMemberType.Items.Add(reader.GetString(0));
                            }
                        }
                    }

                    // Loading cmbMemberStatus.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM member_statuses";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbMemberStatus.Items.Add(reader.GetString(0));
                            }
                        }
                    }

                    // Loading cmbPaymentMethod.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM payment_methods";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbPaymentMethod.Items.Add(reader.GetString(0));
                            }
                        }
                    }

                    // Loading cmbFileType_AddAttachedFile.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT name FROM file_types";

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                cmbFileType_AddAttachedFile.Items.Add(reader.GetString(0));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
                }
                finally
                {
                    pringLoading.IsActive = false;
                }
            }

            InitializeComponent();

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (sender, page) =>
            {
                if (page != null && page == this)
                {
                    m_mainwindow.GoBack();
                    ClearInputs();
                }
            };

            m_dbConn = dbConn;
            m_currentUser = currentUser;

            m_notes = new ObservableCollection<MemberNoteViewModel>();
            lstNotes.ItemsSource = m_notes;

            m_attachedFiles = new ObservableCollection<AttachedFileViewModel>();
            lstAttachedFiles.ItemsSource = m_attachedFiles;

            LoadComboboxesContents();
        }

        public async void Init(Member member)
        {
            ClearInputs();
            m_member = member;

            if (member == null)
            {
                m_createNew = true;
                txtMemberID.IsEnabled = true;
            }
            else
            {
                m_createNew = false;
                pringLoading.IsActive = true;
                txtMemberID.IsEnabled = false;

                try
                {
                    // Getting the personal info.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT members.first_name, members.last_name, members.father_name, members.mother_name, members.gender, members.birth_date, members.birth_location, members.eye_color, members.is_canadian, files.id ";
                        cmd.CommandText += "FROM members ";
                        cmd.CommandText += "INNER JOIN files ON members.profile_image_id = files.id ";
                        cmd.CommandText += "WHERE members.id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", member.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                txtFirstName.Text = reader.GetString(0);
                                txtLastName.Text = (await reader.IsDBNullAsync(1)) ? string.Empty : reader.GetString(1);
                                txtFatherName.Text = (await reader.IsDBNullAsync(2)) ? string.Empty : reader.GetString(2);
                                txtMotherName.Text = (await reader.IsDBNullAsync(3)) ? string.Empty : reader.GetString(3);
                                cmbGender.SelectedItem = reader.GetString(4);
                                dtBirthDate.SelectedDate = new DateTime(reader.GetInt64(5));
                                txtBirthLocation.Text = (await reader.IsDBNullAsync(6)) ? string.Empty : reader.GetString(6);
                                cmbEyeColor.SelectedItem = reader.GetString(7);
                                chkIsCanadian.IsChecked = reader.GetInt32(8) != 0;

                                if (!await reader.IsDBNullAsync(9))
                                {
                                    string imgFileId = reader.GetString(9);
                                    m_member.ProfileImageID = imgFileId; // We need this later!

                                    using (Stream imgStream = AttachedFileAccess.GetFileStream(imgFileId, FileMode.Open))
                                    {
                                        await LoadPhotoFromStream(imgStream);
                                    }
                                }
                            }
                        }
                    }

                    // Getting the address info.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT address_street, address_city, address_local, address_region, address_province, address_postal ";
                        cmd.CommandText += "FROM members WHERE id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", member.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                txtAddressStreet.Text = (await reader.IsDBNullAsync(0)) ? string.Empty : reader.GetString(0);
                                txtAddressCity.Text = (await reader.IsDBNullAsync(1)) ? string.Empty : reader.GetString(1);
                                txtAddressLocal.Text = (await reader.IsDBNullAsync(2)) ? string.Empty : reader.GetString(2);
                                cmbAddressRegion.SelectedItem = (await reader.IsDBNullAsync(3)) ? null : reader.GetString(3);
                                cmbAddressProvince.SelectedItem = (await reader.IsDBNullAsync(4)) ? null : reader.GetString(4);
                                txtAddressPostal.Text = (await reader.IsDBNullAsync(5)) ? string.Empty : reader.GetString(5);
                            }
                        }
                    }

                    // Getting contact info.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT email, home_phone, cell_phone, work_phone FROM members WHERE id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", member.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                txtEmail.Text = (await reader.IsDBNullAsync(0)) ? string.Empty : reader.GetString(0);
                                txtHomePhone.Text = (await reader.IsDBNullAsync(1)) ? string.Empty : reader.GetString(1);
                                txtCellPhone.Text = (await reader.IsDBNullAsync(2)) ? string.Empty : reader.GetString(2);
                                txtWorkPhone.Text = (await reader.IsDBNullAsync(3)) ? string.Empty : reader.GetString(3);
                            }
                        }
                    }

                    // Getting membership info.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT member_since_date, member_type, member_status, payment_method FROM members WHERE id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", member.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                txtMemberID.Text = member.ID;
                                dtMemberSince.SelectedDate = new DateTime(reader.GetInt64(0));
                                cmbMemberType.SelectedItem = reader.GetString(1);
                                cmbMemberStatus.SelectedItem = reader.GetString(2);
                                cmbPaymentMethod.SelectedItem = reader.GetString(3);
                            }
                        }
                    }

                    // Getting the other data.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT members.data_added_date, members.data_modified_date, users.username, members.data_modifier_id ";
                        cmd.CommandText += "FROM members ";
                        cmd.CommandText += "INNER JOIN users ON users.id = members.data_adder_id ";
                        cmd.CommandText += "WHERE members.id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", member.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                txtDataAddedDate.Text = new DateTime(reader.GetInt64(0)).ToString();
                                txtLastModifiedDate.Text = new DateTime(reader.GetInt64(1)).ToString();
                                txtDataAdder.Text = reader.GetString(2);

                                // Getting the username of the last modifier.
                                string modifierId = reader.GetString(3);
                                using (SQLiteCommand modifierCmd = new SQLiteCommand(m_dbConn))
                                {
                                    modifierCmd.CommandText = "SELECT username FROM users WHERE id = @modifierId";
                                    modifierCmd.Parameters.AddWithValue("modifierId", modifierId);

                                    using (DbDataReader modifierReader = await modifierCmd.ExecuteReaderAsync())
                                    {
                                        if (await modifierReader.ReadAsync())
                                        {
                                            txtLastModifier.Text = modifierReader.GetString(0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    // Getting notes.
                    await LoadNotes();

                    // Getting attached files.
                    await LoadAttachedFiles();
                }
                catch (Exception ex)
                {
                    ClearInputs();
                    m_mainwindow.ShowMessage("Error", "Unable to load member info.\nDetails: " + ex.Message);
                    m_mainwindow.GoBack();
                }
                finally
                {
                    pringLoading.IsActive = false;
                }
            }
        }

        void ClearInputs()
        {
            // Personal info.
            txtFirstName.Clear();
            txtLastName.Clear();
            txtFatherName.Clear();
            txtMotherName.Clear();
            cmbGender.SelectedItem = null;
            dtBirthDate.Text = string.Empty;
            txtBirthLocation.Clear();
            cmbEyeColor.SelectedItem = null;
            chkIsCanadian.IsChecked = false;
            imgPhoto.Source = null;

            // Address info.
            txtAddressStreet.Clear();
            txtAddressCity.Clear();
            txtAddressLocal.Clear();
            cmbAddressRegion.SelectedItem = null;
            cmbAddressProvince.SelectedItem = null;
            txtAddressPostal.Clear();

            // Contact info.
            txtEmail.Clear();
            txtHomePhone.Clear();
            txtCellPhone.Clear();
            txtWorkPhone.Clear();

            // Membership info.
            txtMemberID.Clear();
            dtMemberSince.Text = string.Empty;
            cmbMemberType.SelectedItem = null;
            cmbMemberStatus.SelectedItem = null;
            cmbPaymentMethod.SelectedItem = null;

            // Other data.
            txtDataAdder.Clear();
            txtDataAddedDate.Clear();
            txtLastModifier.Clear();
            txtLastModifiedDate.Clear();

            // Notes.
            m_notes.Clear();

            // Attached files.
            m_attachedFiles.Clear();

            // Add note grid.
            gridAddNote.Visibility = Visibility.Hidden;
            txtContent_AddNote.Clear();

            // Add attached file grid.
            gridAddAttachedFile.Visibility = Visibility.Hidden;
            txtFilePath_AddAttachedFile.Clear();
            txtFileName_AddAttachedFile.Clear();
            cmbFileType_AddAttachedFile.SelectedItem = null;
        }

        async Task LoadPhotoFromStream(Stream imgStream)
        {
            BitmapImage img = await Imaging.LoadImageFromStreamAsync(imgStream, 500);
            imgPhoto.Source = img;
        }

        async Task LoadNotes()
        {
            m_notes.Clear();

            if (m_member == null)
            {
                throw new Exception("Member is null");
            }

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT notes.id, notes.content, notes.date_created, users.username FROM members_notes_link ";
                cmd.CommandText += "INNER JOIN notes ON members_notes_link.note_id = notes.id ";
                cmd.CommandText += "INNER JOIN users ON notes.creator_id = users.id ";
                cmd.CommandText += "WHERE members_notes_link.member_id = @memberId";
                cmd.Parameters.AddWithValue("memberId", m_member.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        MemberNote note = new MemberNote();
                        note.ID = reader.GetString(0);
                        note.Content = reader.GetString(1);
                        note.DateCreated = new DateTime(reader.GetInt64(2));
                        note.CreatorUserName = reader.GetString(3);
                        m_notes.Add(new MemberNoteViewModel(note));
                    }
                }
            }
        }

        async Task LoadAttachedFiles()
        {
            m_attachedFiles.Clear();

            if (m_member == null)
            {
                throw new Exception("Member is null");
            }

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT members_files_link.file_id, files.original_name, members_files_link.type, members_files_link.added_time, members_files_link.last_modified_time, users.username, members_files_link.modifier_id "; // users.username is the adder username
                cmd.CommandText += "FROM members_files_link ";
                cmd.CommandText += "INNER JOIN files ON files.id = members_files_link.file_id ";
                cmd.CommandText += "INNER JOIN users ON users.id = members_files_link.adder_id ";
                cmd.CommandText += "WHERE member_id = @memberId";
                cmd.Parameters.AddWithValue("memberId", m_member.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        AttachedFile file = new AttachedFile();
                        file.ID = reader.GetString(0);
                        file.OriginalFileName = reader.GetString(1);
                        file.Type = reader.GetString(2);
                        file.AddedTime = new DateTime(reader.GetInt64(3));
                        file.LastModifiedTime = new DateTime(reader.GetInt64(4));
                        file.AdderUserName = reader.GetString(5);

                        // Getting last modifier username.
                        string modifierId = reader.GetString(6);
                        using (SQLiteCommand userCmd = new SQLiteCommand(m_dbConn))
                        {
                            userCmd.CommandText = "SELECT username FROM users WHERE id = @modifierId";
                            userCmd.Parameters.AddWithValue("modifierId", modifierId);

                            using (DbDataReader userReader = await userCmd.ExecuteReaderAsync())
                            {
                                if (await userReader.ReadAsync())
                                {
                                    file.LastModifierUserName = userReader.GetString(0);
                                }
                            }
                        }

                        // Getting file size.
                        using (Stream fs = AttachedFileAccess.GetFileStream(file.ID, FileMode.Open))
                        {
                            file.Size = fs.Length;
                        }

                        // Done.
                        m_attachedFiles.Add(new AttachedFileViewModel(file));
                    }
                }
            }
        }

        private async void imgPhoto_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select the latest photo of the member...";
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            ofd.FileName = string.Empty;
            ofd.Filter = "Image Files|*.png;*.jpg;*.jpeg;*.bmp;";
            bool? result = ofd.ShowDialog();

            if (!(result.HasValue && result.Value))
            {
                return;
            }

            // Saving the original path.
            m_photoOriginalPathTemp = ofd.FileName;

            // Loading the photo.
            using (Stream imgStream = File.Open(m_photoOriginalPathTemp, FileMode.Open))
            {
                await LoadPhotoFromStream(imgStream);
            }
        }

        private void btnAddNote_Click(object sender, RoutedEventArgs e)
        {
            gridAddNote.Visibility = Visibility.Visible;
        }

        private async void btnDeleteNote_Click(object sender, RoutedEventArgs e)
        {
            MemberNoteViewModel selitem;
            if ((selitem = lstNotes.SelectedItem as MemberNoteViewModel) != null)
            {
                btnAddNote.IsEnabled = false;
                btnDeleteNote.IsEnabled = false;

                try
                {
                    if (m_createNew)
                    {
                        m_notes.Remove(selitem);
                    }
                    else
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "DELETE FROM notes WHERE id = @noteId";
                            cmd.Parameters.AddWithValue("noteId", selitem.Model.ID);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete a note.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnAddNote.IsEnabled = true;
                    btnDeleteNote.IsEnabled = true;
                    await LoadNotes();
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No Note Selected", "Please select a note from the list first!");
            }
        }

        private async void btnOK_AddNote_Click(object sender, RoutedEventArgs e)
        {
            btnOK_AddNote.IsEnabled = false;
            btnCancel_AddNote.IsEnabled = false;

            try
            {
                if (string.IsNullOrEmpty(txtContent_AddNote.Text))
                {
                    throw new Exception("Note content is null");
                }

                string noteId = await Randomizer.RandomStringAsync(32);

                if (m_createNew)
                {
                    MemberNote note = new MemberNote();
                    note.ID = noteId;
                    note.Content = txtContent_AddNote.Text;
                    note.DateCreated = DateTime.Now;
                    note.CreatorUserName = m_currentUser.UserName;
                    m_notes.Add(new MemberNoteViewModel(note));
                }
                else
                {
                    // Inserting into notes table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO notes(id, content, creator_id, date_created) VALUES(@noteId, @content, @creatorId, @dateCreatedTicks)";
                        cmd.Parameters.AddWithValue("noteId", noteId);
                        cmd.Parameters.AddWithValue("content", txtContent_AddNote.Text);
                        cmd.Parameters.AddWithValue("creatorId", m_currentUser.ID);
                        cmd.Parameters.AddWithValue("dateCreatedTicks", DateTime.Now.Ticks);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Linking the note with member.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO members_notes_link(member_id, note_id) VALUES(@memberId, @noteId)";
                        cmd.Parameters.AddWithValue("memberId", m_member.ID);
                        cmd.Parameters.AddWithValue("noteId", noteId);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Reloading notes list.
                    await LoadNotes();
                }

                txtContent_AddNote.Clear();
                gridAddNote.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add a new note.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_AddNote.IsEnabled = true;
                btnCancel_AddNote.IsEnabled = true;
            }
        }

        private void btnCancel_AddNote_Click(object sender, RoutedEventArgs e)
        {
            txtContent_AddNote.Clear();
            gridAddNote.Visibility = Visibility.Hidden;
        }

        private void btnAddAttachedFile_Click(object sender, RoutedEventArgs e)
        {
            gridAddAttachedFile.Visibility = Visibility.Visible;
        }

        private async void btnOK_AddAttachedFile_Click(object sender, RoutedEventArgs e)
        {
            btnAddAttachedFile.IsEnabled = false;
            pringLoading.IsActive = true;

            try
            {
                if (string.IsNullOrEmpty(txtFilePath_AddAttachedFile.Text))
                {
                    throw new Exception("No file selected");
                }

                if (cmbFileType_AddAttachedFile.SelectedItem == null)
                {
                    throw new Exception("No file type specified");
                }

                FileInfo finfo = new FileInfo(txtFilePath_AddAttachedFile.Text);
                string originalFileName = txtFileName_AddAttachedFile.Text;

                if (string.IsNullOrEmpty(originalFileName))
                {
                    throw new Exception("File name is null");
                }

                // Looking for duplicate file name;
                for (int i = 0; i < m_attachedFiles.Count; ++i)
                {
                    if (m_attachedFiles[i].OriginalFileName == originalFileName)
                    {
                        throw new Exception("Duplicate file name found");
                    }
                }

                if (finfo.Length > 15 * 1024 * 1024) // 15 MB
                {
                    m_mainwindow.ShowMessage("Error", string.Format("File '{0}' has size more than 15 MB!", Path.GetFileName(finfo.FullName)));
                }
                else
                {
                    AttachedFile file = new AttachedFile();
                    file.ID = await Randomizer.RandomStringAsync(32);
                    file.OriginalPath = finfo.FullName;
                    file.OriginalFileName = originalFileName;
                    file.Size = finfo.Length;
                    file.Type = cmbFileType_AddAttachedFile.SelectedItem as string;
                    file.AddedTime = DateTime.Now;
                    file.LastModifiedTime = DateTime.Now;
                    file.AdderUserName = m_currentUser.UserName;
                    file.LastModifierUserName = m_currentUser.UserName;

                    if (m_createNew) // Just add a new instance.
                    {
                        m_attachedFiles.Add(new AttachedFileViewModel(file));
                    }
                    else // Also copy the file!
                    {
                        // Copying the file.
                        await AttachedFileAccess.CopyFileInto(finfo.FullName, file.ID);

                        // Adding to files table.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "INSERT INTO files(id, original_name) VALUES(@fileId, @originalFileName)";
                            cmd.Parameters.AddWithValue("fileId", file.ID);
                            cmd.Parameters.AddWithValue("originalFileName", file.OriginalFileName);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Adding to the link table.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "INSERT INTO members_files_link(file_id, member_id, type, added_time, last_modified_time, adder_id, modifier_id) VALUES(@fileId, @memberId, @fileType, @timeNowTicks, @timeNowTicks, @currentUserId, @currentUserId)";
                            cmd.Parameters.AddWithValue("fileId", file.ID);
                            cmd.Parameters.AddWithValue("memberId", m_member.ID);
                            cmd.Parameters.AddWithValue("fileType", file.Type);
                            cmd.Parameters.AddWithValue("timeNowTicks", DateTime.Now.Ticks);
                            cmd.Parameters.AddWithValue("currentUserId", m_currentUser.ID);
                            await cmd.ExecuteNonQueryAsync();
                        }
                        
                        await LoadAttachedFiles();
                    }

                    txtFilePath_AddAttachedFile.Clear();
                    cmbFileType_AddAttachedFile.SelectedItem = null;
                    gridAddAttachedFile.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add a file.\nDetails: " + ex.Message);
            }
            finally
            {
                btnAddAttachedFile.IsEnabled = true;
                pringLoading.IsActive = false;
            }
        }

        private void btnCancel_AddAttachedFile_Click(object sender, RoutedEventArgs e)
        {
            txtFilePath_AddAttachedFile.Clear();
            cmbFileType_AddAttachedFile.SelectedItem = null;
            gridAddAttachedFile.Visibility = Visibility.Hidden;
        }

        private void btnBrowse_AddAttachedFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select files to attach...";
            ofd.FileName = string.Empty;
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            bool? result = ofd.ShowDialog();

            if (result.HasValue && result.Value)
            {
                txtFilePath_AddAttachedFile.Text = ofd.FileName;
                txtFileName_AddAttachedFile.Text = Path.GetFileName(ofd.FileName);
            }
        }

        private async void btnRemoveAttachedFile_Click(object sender, RoutedEventArgs e)
        {
            AttachedFileViewModel selitem;
            if ((selitem = lstAttachedFiles.SelectedItem as AttachedFileViewModel) != null)
            {
                if (m_createNew)
                {
                    m_attachedFiles.Remove(selitem);
                }
                else
                {
                    btnRemoveAttachedFile.IsEnabled = false;

                    try
                    {
                        // Removing from the link table.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "DELETE FROM members_files_link WHERE member_id = @memberId AND file_id = @fileId";
                            cmd.Parameters.AddWithValue("memberId", m_member.ID);
                            cmd.Parameters.AddWithValue("fileId", selitem.Model.ID);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Removing from files table.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "DELETE FROM files WHERE id = @fileId";
                            cmd.Parameters.AddWithValue("fileId", selitem.Model.ID);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Deleting the file.
                        AttachedFileAccess.DeleteFile(selitem.Model.ID);

                        // Done.
                        await LoadAttachedFiles();
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to remove an attached file.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        btnRemoveAttachedFile.IsEnabled = false;
                    }
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No File Selected", "Please select a file from the list!");
            }
        }

        private async void btnOpenAttachedFile_Click(object sender, RoutedEventArgs e)
        {
            AttachedFileViewModel selitem;
            if ((selitem = lstAttachedFiles.SelectedItem as AttachedFileViewModel) != null)
            {
                try
                {
                    string attachedFileDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\MMF\\temp\\";

                    if (!Directory.Exists(attachedFileDir))
                    {
                        // Creating the temp dir.
                        Directory.CreateDirectory(attachedFileDir);
                    }

                    string filePath = string.Empty;

                    if (m_createNew)
                    {
                        // Opening the original file instead.
                        filePath = selitem.Model.OriginalPath;
                    }
                    else
                    {
                        // Copying the file to the temp dir.
                        filePath = Path.Combine(attachedFileDir, string.Format("{0}_{1}", m_member.ID, selitem.Model.OriginalFileName));

                        using (Stream input = AttachedFileAccess.GetFileStream(selitem.Model.ID, FileMode.Open))
                        using (Stream output = File.Open(filePath, FileMode.Create))
                        {
                            await input.CopyToAsync(output);
                        }
                    }

                    // Opening the file.
                    Process p = Process.Start(filePath);
                    await Task.Delay(5000);

                    // Waiting for the default app to close.
                    while (p != null && !p.HasExited)
                    {
                        await Task.Delay(1000);
                    }

                    if (!m_createNew)
                    {
                        // Checking for file lock.
                        while (true)
                        {
                            try
                            {
                                using (Stream f = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                                {
                                    break;
                                }
                            }
                            catch
                            {
                                await Task.Delay(1000);
                            }
                        }

                        // Checking for file changes.
                        string originalMD5 = string.Empty;
                        string editedMD5 = string.Empty;
                        
                        using (Stream originalStream = AttachedFileAccess.GetFileStream(selitem.Model.ID, FileMode.Open))
                        using (Stream editedStream = File.Open(filePath, FileMode.Open))
                        using (MD5 md5 = MD5.Create())
                        {
                            // This can be a very long operation.
                            await Task.Run(() =>
                            {
                                originalMD5 = BitConverter.ToString(md5.ComputeHash(originalStream)).Replace("-", string.Empty);
                                editedMD5 = BitConverter.ToString(md5.ComputeHash(editedStream)).Replace("-", string.Empty);
                            });
                        }

                        if (editedMD5 != originalMD5)
                        {
                            // Changes detected.
                            // Copying back the file to the database.
                            await AttachedFileAccess.CopyFileInto(filePath, selitem.Model.ID);

                            // Saving the editor and last edited time.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "UPDATE members_files_link SET modifier_id = @modifierId, last_modified_time = @lastModifiedTimeTicks WHERE member_id = @memberId AND file_id = @fileId";
                                cmd.Parameters.AddWithValue("modifierId", m_currentUser.ID);
                                cmd.Parameters.AddWithValue("lastModifiedTimeTicks", DateTime.Now.Ticks);
                                cmd.Parameters.AddWithValue("memberId", m_member.ID);
                                cmd.Parameters.AddWithValue("fileId", selitem.Model.ID);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Notifying the user.
                            m_mainwindow.ShowMessage("Info", string.Format("Changes of file {0} was saved to the database.", selitem.Model.OriginalFileName));

                            // Refreshing the attached files list.
                            await LoadAttachedFiles();
                        }

                        // Deleting the temp file.
                        File.Delete(filePath);
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to process selected file.\nDetails: " + ex.Message);
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No File Selected", "Please select a file from the list!");
            }
        }

        private async void btnApply_Click(object sender, RoutedEventArgs e)
        {
            DateTime now = DateTime.Now; // Cache the current time value.
            btnApply.IsEnabled = false;
            tbctrlMain.IsEnabled = false;
            pringLoading.IsActive = true;

            try
            {
                // Checking the user type.
                if (m_currentUser.Type != "ADMINISTRATOR" && m_currentUser.Type != "MANAGER")
                {
                    throw new Exception("Insufficient permission");
                }

                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtFirstName.Text))
                {
                    throw new Exception("First name is empty");
                }

                if (!dtBirthDate.SelectedDate.HasValue)
                {
                    throw new Exception("No date selected for birth");
                }

                if (cmbGender.SelectedItem == null)
                {
                    throw new Exception("No gender selected");
                }

                if (cmbEyeColor.SelectedItem == null)
                {
                    throw new Exception("No eye color selected");
                }

                if (!dtMemberSince.SelectedDate.HasValue)
                {
                    throw new Exception("No date selected for member since");
                }

                if (cmbMemberStatus.SelectedItem == null)
                {
                    throw new Exception("No member status selected");
                }

                if (cmbMemberType.SelectedItem == null)
                {
                    throw new Exception("No member type selected");
                }

                if (cmbPaymentMethod.SelectedItem == null)
                {
                    throw new Exception("No payment method selected");
                }

                using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                {
                    if (m_createNew)
                    {
                        // Creating member instance in the database.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            // Saving personal info.
                            cmd.CommandText = "INSERT INTO members(";
                            cmd.CommandText += "first_name, last_name, father_name, mother_name, birth_date, birth_location, gender, eye_color, is_canadian, "; // Personal info.
                            cmd.CommandText += "address_street, address_city, address_local, address_region, address_province, address_postal, "; // Address info.
                            cmd.CommandText += "email, home_phone, cell_phone, work_phone, "; // Contact info.
                            cmd.CommandText += "id, member_since_date, member_status, member_type, payment_method, "; // Membership info.
                            cmd.CommandText += "data_added_date, data_adder_id, data_modified_date, data_modifier_id) "; // Other info.
                            cmd.CommandText += "VALUES(";
                            cmd.CommandText += "@firstName, @lastName, @fatherName, @motherName, @birthDateTicks, @birthLocation, @gender, @eyeColor, @isCanadian, ";
                            cmd.CommandText += "@addressStreet, @addressCity, @addressLocal, @addressRegion, @addressProvince, @addressPostal, ";
                            cmd.CommandText += "@email, @homePhone, @cellPhone, @workPhone, ";
                            cmd.CommandText += "@memberId, @memberSinceDateTicks, @memberStatus, @memberType, @paymentMethod, ";
                            cmd.CommandText += "@timeNowTicks, @currentUserId, @timeNowTicks, @currentUserId)";

                            // Parameters of personal info.
                            cmd.Parameters.AddWithValue("firstName", txtFirstName.Text);
                            cmd.Parameters.AddWithValue("lastName", txtLastName.Text);
                            cmd.Parameters.AddWithValue("fatherName", txtFatherName.Text);
                            cmd.Parameters.AddWithValue("motherName", txtMotherName.Text);
                            cmd.Parameters.AddWithValue("birthDateTicks", dtBirthDate.SelectedDate.Value.Ticks);
                            cmd.Parameters.AddWithValue("birthLocation", txtBirthLocation.Text);
                            cmd.Parameters.AddWithValue("gender", cmbGender.SelectedItem as string);
                            cmd.Parameters.AddWithValue("eyeColor", cmbEyeColor.SelectedItem as string);
                            cmd.Parameters.AddWithValue("isCanadian", chkIsCanadian.IsChecked.Value ? 1 : 0);

                            // Parameters of address info.
                            cmd.Parameters.AddWithValue("addressStreet", txtAddressStreet.Text);
                            cmd.Parameters.AddWithValue("addressCity", txtAddressCity.Text);
                            cmd.Parameters.AddWithValue("addressLocal", txtAddressLocal.Text);
                            cmd.Parameters.AddWithValue("addressRegion", cmbAddressRegion.SelectedItem as string);
                            cmd.Parameters.AddWithValue("addressProvince", cmbAddressProvince.SelectedItem as string);
                            cmd.Parameters.AddWithValue("addressPostal", txtAddressPostal.Text);

                            // Parameters of contact info.
                            cmd.Parameters.AddWithValue("email", txtEmail.Text);
                            cmd.Parameters.AddWithValue("homePhone", txtHomePhone.Text);
                            cmd.Parameters.AddWithValue("cellPhone", txtCellPhone.Text);
                            cmd.Parameters.AddWithValue("workPhone", txtWorkPhone.Text);

                            // Parameters of membership info.
                            cmd.Parameters.AddWithValue("memberId", txtMemberID.Text);
                            cmd.Parameters.AddWithValue("memberSinceDateTicks", dtMemberSince.SelectedDate.Value.Ticks);
                            cmd.Parameters.AddWithValue("memberStatus", cmbMemberStatus.SelectedItem as string);
                            cmd.Parameters.AddWithValue("memberType", cmbMemberType.SelectedItem as string);
                            cmd.Parameters.AddWithValue("paymentMethod", cmbPaymentMethod.SelectedItem as string);

                            // Parameters of other info.
                            cmd.Parameters.AddWithValue("timeNowTicks", now.Ticks);
                            cmd.Parameters.AddWithValue("currentUserId", m_currentUser.ID);

                            // Executing query.
                            await cmd.ExecuteNonQueryAsync();
                        }

                        if (!string.IsNullOrEmpty(m_photoOriginalPathTemp)) // else no photo for this member.
                        {
                            // Adding member photo.
                            // Copying the photo file.
                            string photoId = await Randomizer.RandomStringAsync(32);
                            await AttachedFileAccess.CopyFileInto(m_photoOriginalPathTemp, photoId);

                            // Adding to the files table.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "INSERT INTO files(id, original_name) VALUES(@photoId, @originalName)";
                                cmd.Parameters.AddWithValue("photoId", photoId);
                                cmd.Parameters.AddWithValue("originalName", "photo_" + txtMemberID.Text); // This wont be used anyway.
                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Adding to the members table.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "UPDATE members SET profile_image_id = @photoId WHERE id = @memberId";
                                cmd.Parameters.AddWithValue("photoId", photoId);
                                cmd.Parameters.AddWithValue("memberId", txtMemberID.Text);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }

                        // Saving the notes.
                        for (int i = 0; i < lstNotes.Items.Count; ++i)
                        {
                            MemberNote note = (lstNotes.Items[i] as MemberNoteViewModel).Model;

                            // Adding to notes table.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "INSERT INTO notes(id, content, creator_id, date_created) VALUES(@noteId, @content, @currentUserId, @timeNowTicks)";
                                cmd.Parameters.AddWithValue("noteId", note.ID);
                                cmd.Parameters.AddWithValue("content", note.Content);
                                cmd.Parameters.AddWithValue("currentUserId", m_currentUser.ID);
                                cmd.Parameters.AddWithValue("timeNowTicks", now.Ticks);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Adding to the link table.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "INSERT INTO members_notes_link(member_id, note_id) VALUES(@memberId, @noteId)";
                                cmd.Parameters.AddWithValue("memberId", txtMemberID.Text);
                                cmd.Parameters.AddWithValue("noteId", note.ID);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }

                        // Saving attached files.
                        for (int i = 0; i < lstAttachedFiles.Items.Count; ++i)
                        {
                            AttachedFile file = (lstAttachedFiles.Items[i] as AttachedFileViewModel).Model;

                            // Copying the file.
                            await AttachedFileAccess.CopyFileInto(file.OriginalPath, file.ID);

                            // Adding to files table.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "INSERT INTO files(id, original_name) VALUES(@fileId, @originalName)";
                                cmd.Parameters.AddWithValue("fileId", file.ID);
                                cmd.Parameters.AddWithValue("originalName", file.OriginalFileName);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Adding to the link table.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "INSERT INTO members_files_link(file_id, member_id, type, added_time, last_modified_time, adder_id, modifier_id) ";
                                cmd.CommandText += "VALUES(@fileId, @memberId, @fileType, @timeNowTicks, @timeNowTicks, @currentUserId, @currentUserId)";
                                cmd.Parameters.AddWithValue("fileId", file.ID);
                                cmd.Parameters.AddWithValue("memberId", txtMemberID.Text);
                                cmd.Parameters.AddWithValue("fileType", file.Type);
                                cmd.Parameters.AddWithValue("timeNowTicks", now.Ticks);
                                cmd.Parameters.AddWithValue("currentUserId", m_currentUser.ID);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }

                        m_mainwindow.ShowMessage("Info", "A new member has been added.\nAll member info was saved.");
                        
                        // Commiting changes.
                        transaction.Commit();

                        // Reloading the values.
                        string memberId = txtMemberID.Text; // Caching the value before clearing the inputs.
                        Init(new Member() { ID = memberId }); // Init actually just need the ID.
                    }
                    else
                    {
                        // Saving info.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            // WARNING: Member ID cannot be changed!
                            cmd.CommandText = "UPDATE members SET ";
                            cmd.CommandText += "first_name = @firstName, last_name = @lastName, father_name = @fatherName, mother_name = @motherName, birth_date = @birthDateTicks, birth_location = @birthLocation, gender = @gender, eye_color = @eyeColor, is_canadian = @isCanadian, ";
                            cmd.CommandText += "address_street = @addressStreet, address_city = @addressCity, address_local = @addressLocal, address_region = @addressRegion, address_province = @addressProvince, address_postal = @addressPostal, ";
                            cmd.CommandText += "email = @email, home_phone = @homePhone, cell_phone = @cellPhone, work_phone = @workPhone, ";
                            cmd.CommandText += "member_since_date = @memberSinceDateTicks, member_status = @memberStatus, member_type = @memberType, payment_method = @paymentMethod, ";
                            cmd.CommandText += "data_modified_date = @timeNowTicks, data_modifier_id = @currentUserId ";
                            cmd.CommandText += "WHERE id = @memberId";

                            // Personal info.
                            cmd.Parameters.AddWithValue("firstName", txtFirstName.Text);
                            cmd.Parameters.AddWithValue("lastName", txtLastName.Text);
                            cmd.Parameters.AddWithValue("fatherName", txtFatherName.Text);
                            cmd.Parameters.AddWithValue("motherName", txtMotherName.Text);
                            cmd.Parameters.AddWithValue("birthDateTicks", dtBirthDate.SelectedDate.Value.Ticks);
                            cmd.Parameters.AddWithValue("birthLocation", txtBirthLocation.Text);
                            cmd.Parameters.AddWithValue("gender", cmbGender.SelectedItem as string);
                            cmd.Parameters.AddWithValue("eyeColor", cmbEyeColor.SelectedItem as string);
                            cmd.Parameters.AddWithValue("isCanadian", chkIsCanadian.IsChecked.Value ? 1 : 0);

                            // Address info.
                            cmd.Parameters.AddWithValue("addressStreet", txtAddressStreet.Text);
                            cmd.Parameters.AddWithValue("addressCity", txtAddressCity.Text);
                            cmd.Parameters.AddWithValue("addressLocal", txtAddressLocal.Text);
                            cmd.Parameters.AddWithValue("addressRegion", cmbAddressRegion.SelectedItem as string);
                            cmd.Parameters.AddWithValue("addressProvince", cmbAddressProvince.SelectedItem as string);
                            cmd.Parameters.AddWithValue("addressPostal", txtAddressPostal.Text);

                            // Contact info.
                            cmd.Parameters.AddWithValue("email", txtEmail.Text);
                            cmd.Parameters.AddWithValue("homePhone", txtHomePhone.Text);
                            cmd.Parameters.AddWithValue("cellPhone", txtCellPhone.Text);
                            cmd.Parameters.AddWithValue("workPhone", txtWorkPhone.Text);

                            // Membership info.
                            cmd.Parameters.AddWithValue("memberId", m_member.ID);
                            cmd.Parameters.AddWithValue("memberSinceDateTicks", dtMemberSince.SelectedDate.Value.Ticks);
                            cmd.Parameters.AddWithValue("memberStatus", cmbMemberStatus.SelectedItem as string);
                            cmd.Parameters.AddWithValue("memberType", cmbMemberType.SelectedItem as string);
                            cmd.Parameters.AddWithValue("paymentMethod", cmbPaymentMethod.SelectedItem as string);

                            // Other info.
                            cmd.Parameters.AddWithValue("timeNowTicks", now.Ticks);
                            cmd.Parameters.AddWithValue("currentUserId", m_currentUser.ID);

                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Saving the photo.
                        if (!string.IsNullOrEmpty(m_photoOriginalPathTemp)) // else no changes of the photo.
                        {
                            if (string.IsNullOrEmpty(m_member.ProfileImageID)) // No image has been added.
                            {
                                string photoId = await Randomizer.RandomStringAsync(32);

                                // Copying the file.
                                await AttachedFileAccess.CopyFileInto(m_photoOriginalPathTemp, photoId);

                                // Saving to files table.
                                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                                {
                                    cmd.CommandText = "INSERT INTO files(id, original_name) VALUES(@photoId, @originalName)";
                                    cmd.Parameters.AddWithValue("photoId", photoId);
                                    cmd.Parameters.AddWithValue("originalName", "photo_" + m_member.ID);
                                    await cmd.ExecuteNonQueryAsync();
                                }

                                // Saving to members table.
                                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                                {
                                    cmd.CommandText = "UPDATE members SET profile_image_id = @photoId WHERE id = @memberId";
                                    cmd.Parameters.AddWithValue("photoId", photoId);
                                    cmd.Parameters.AddWithValue("memberId", m_member.ID);
                                    await cmd.ExecuteNonQueryAsync();
                                }
                            }
                            else // Just copy the file.
                            {
                                await AttachedFileAccess.CopyFileInto(m_photoOriginalPathTemp, m_member.ProfileImageID);
                            }
                        }

                        // Commiting changes.
                        transaction.Commit();

                        // Refresing the values.
                        Init(m_member);

                        m_mainwindow.ShowMessage("Info", "Changes of the member have been saved.");
                    }

                    MembersChanged?.Invoke();
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnApply.IsEnabled = true;
                tbctrlMain.IsEnabled = true;
                pringLoading.IsActive = false;
            }
        }
    }
}
