﻿using member_manager.Commons;
using System;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace member_manager.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        const string DB_FILE_NAME = "DB";

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;


        public LoginPage(MainWindow mainwindow)
        {
            async void Init()
            {
                pringLoading.IsActive = true;

                if (!File.Exists(DB_FILE_NAME))
                {
                    // Creating db file.
                    SQLiteConnection.CreateFile(DB_FILE_NAME);

                    // Opening db connection.
                    m_dbConn = new SQLiteConnection(string.Format("Data source={0};", DB_FILE_NAME));
                    await m_dbConn.OpenAsync();

                    // Executing init.sql
                    using (StreamReader reader = new StreamReader(File.Open("Resources\\Scripts\\init.sql", FileMode.Open)))
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = await reader.ReadToEndAsync();
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    // Adding admin user.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO users(id, username, password, first_name, user_type, last_login) VALUES(@userId, 'admin', 'admin', 'Admin', 'ADMINISTRATOR', 0)";
                        cmd.Parameters.AddWithValue("userId", await Randomizer.RandomStringAsync(32));
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                else
                {
                    // Opening db connection.
                    m_dbConn = new SQLiteConnection(string.Format("Data source={0};", DB_FILE_NAME));
                    await m_dbConn.OpenAsync();
                }

                // Turning on the foreign keys enforcement.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    cmd.CommandText = "PRAGMA foreign_keys = 1";
                    await cmd.ExecuteNonQueryAsync();
                }

                pringLoading.IsActive = false;
            }

            InitializeComponent();

            m_mainwindow = mainwindow;
            txtUsername.Focus();

            Init();
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            txtUsername.IsEnabled = false;
            txtPassword.IsEnabled = false;
            btnLogin.IsEnabled = false;
            pringLoading.IsActive = true;

            try
            {
                while (m_dbConn.State != System.Data.ConnectionState.Open)
                {
                    await Task.Delay(100);
                }

                string username = txtUsername.Text;
                string password = txtPassword.Password;

                if (string.IsNullOrEmpty(username))
                {
                    throw new Exception("Username is null");
                }

                if (string.IsNullOrEmpty(password))
                {
                    throw new Exception("Password is null");
                }

                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    cmd.CommandText = "SELECT id FROM users WHERE username = @username AND password = @password";
                    cmd.Parameters.AddWithValue("username", username);
                    cmd.Parameters.AddWithValue("password", password);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            string id = reader.GetString(0);

                            // Clearing login input.
                            txtUsername.Clear();
                            txtPassword.Clear();

                            // Redirecting to main page.
                            MainPage mainPage = new MainPage(m_mainwindow, m_dbConn, id);
                            m_mainwindow.Navigate(mainPage);
                        }
                        else
                        {
                            throw new Exception("Invalid username and password combination");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to login.\nDetails: " + ex.Message);
            }
            finally
            {
                txtUsername.IsEnabled = true;
                txtPassword.IsEnabled = true;
                btnLogin.IsEnabled = true;
                pringLoading.IsActive = false;
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnLogin_Click(null, null);
            }
        }
    }
}
