﻿using System;
using System.Collections.Generic;

namespace member_manager.Models
{
    public enum Gender
    {
        Male,
        Female
    }

    public class Member
    {
        public string ID { get; set; }

        public string Email { get; set; }

        public string ProfileImageID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public DateTime BirthDate { get; set; }

        public string BirthLocation { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string EyeColor { get; set; }

        public string HomePhone { get; set; }

        public string CellPhone { get; set; }

        public string WorkPhone { get; set; }

        public string AddressStreet { get; set; }

        public string AddressCity { get; set; }

        public string AddressLocal { get; set; }

        public string AddressRegion { get; set; }

        public string AddressProvince { get; set; }

        public string AddressPostal { get; set; }

        public DateTime MemberSinceDate { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string PaymentMethod { get; set; }

        public bool IsCanadian { get; set; }

        public DateTime DataAddedDate { get; set; }

        public DateTime LastEditedDate { get; set; }

        public string AdderUserName { get; set; }

        public string LastModifierUserName { get; set; }

        public List<AttachedFile> AttachedFiles { get; set; }


        public override bool Equals(object obj)
        {
            Member comp;
            if ((comp = obj as Member) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
