﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace member_manager.Commons
{
    public static class Randomizer
    {
        const string m_strings = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_&@";
        static readonly Random random = new Random();

        public static Task<string> RandomStringAsync(int length)
        {
            return Task.Run(() => RandomString(length));
        }

        public static string RandomString(int length)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; ++i)
            {
                char ch = m_strings[random.Next(m_strings.Length)];
                sb.Append(ch);
            }
            return sb.ToString();
        }
    }
}
