﻿using member_manager.Commons;
using member_manager.Models;
using member_manager.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace member_manager.Views
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        MainWindow m_mainwindow;
        UserPage m_userPage;
        MemberPage m_memberPage;
        SQLiteConnection m_dbConn;
        User m_user;


        public MainPage(MainWindow mainwindow, SQLiteConnection dbConn, string userId)
        {
            async void Init()
            {
                try
                {
                    // Getting all users.
                    List<UserViewModel> users = await LoadUsers(string.Empty);
                    lstUsers.ItemsSource = users;

                    // Getting current user info.
                    for (int i = 0; i < users.Count; ++i)
                    {
                        if (users[i].Model.ID == userId)
                        {
                            m_user = users[i].Model;
                            m_user.LastLogin = DateTime.Now;
                            m_mainwindow.Title = string.Format("{0} - MMF Membership System", m_user.UserName);
                            break;
                        }
                    }

                    // Applying last login info.
                    using (SQLiteCommand cmd = new SQLiteCommand(dbConn))
                    {
                        cmd.CommandText = "UPDATE users SET last_login = @lastLoginTicks WHERE id = @userId";
                        cmd.Parameters.AddWithValue("userId", userId);
                        cmd.Parameters.AddWithValue("lastLoginTicks", m_user.LastLogin.Ticks);

                        await cmd.ExecuteNonQueryAsync();
                        Console.WriteLine("Success update last login of user: {0}", m_user.UserName);
                    }

                    // Getting all members.
                    List<MemberViewModel> members = await LoadMembers(string.Empty);
                    lstMembers.ItemsSource = members;
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Error when initializing. Please restart the app!\nDetails: " + ex.Message);
                }
            }

            InitializeComponent();

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (sender, page) =>
            {
                if (page != null && page == this)
                {
                    // Logging out.
                    m_mainwindow.GoBack();

                    m_dbConn = null;
                    m_user = null;
                    m_memberPage = null;
                    m_userPage = null;
                    m_mainwindow = null;
                }
            };

            m_dbConn = dbConn;

            // Initializing.
            Init();
        }

        async Task<List<UserViewModel>> LoadUsers(string filter)
        {
            List<UserViewModel> users = new List<UserViewModel>();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = string.Format("SELECT id, username, password, first_name, last_name, email, last_login, user_type FROM users WHERE username LIKE '{0}%' OR first_name LIKE '{0}%' OR last_name LIKE '{0}%' OR email LIKE '{0}%' OR user_type LIKE '{0}%'", filter ?? string.Empty);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        User user = new User();
                        user.ID = reader.GetString(0);
                        user.UserName = reader.GetString(1);
                        user.Password = reader.GetString(2);
                        user.FirstName = reader.GetString(3);
                        user.LastName = await reader.IsDBNullAsync(4) ? string.Empty : reader.GetString(4);
                        user.Email = await reader.IsDBNullAsync(5) ? string.Empty : reader.GetString(5);
                        user.LastLogin = new DateTime(reader.GetInt64(6));
                        user.Type = reader.GetString(7);
                        users.Add(new UserViewModel(user));
                    }
                }
            }

            return users;
        }

        async Task<List<MemberViewModel>> LoadMembers(string filter)
        {
            List<MemberViewModel> members = new List<MemberViewModel>();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT id, first_name, last_name, gender, address_region, address_province, member_since_date, member_status ";
                cmd.CommandText += "FROM members ";
                cmd.CommandText += "WHERE id LIKE '{0}%' OR first_name LIKE '{0}%' OR last_name LIKE '{0}%' OR gender LIKE '{0}%' OR address_region LIKE '{0}%' OR address_province LIKE '{0}%' OR member_status LIKE '{0}%'";
                cmd.CommandText = string.Format(cmd.CommandText, filter ?? string.Empty);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        // Getting member's basic info.
                        // The rest of member's info will be loaded at the member page instead.
                        Member member = new Member();
                        member.ID = reader.GetString(0);
                        member.FirstName = reader.GetString(1);
                        member.LastName = await reader.IsDBNullAsync(2) ? string.Empty : reader.GetString(2);
                        member.Gender = reader.GetString(3) == "MALE" ? Gender.Male : Gender.Female;
                        member.AddressRegion = await reader.IsDBNullAsync(4) ? string.Empty : reader.GetString(4);
                        member.AddressProvince = await reader.IsDBNullAsync(5) ? string.Empty : reader.GetString(5);
                        member.MemberSinceDate = new DateTime(reader.GetInt64(6));
                        member.Status = reader.GetString(7);
                        members.Add(new MemberViewModel(member));
                    }
                }
            }

            return members;
        }

        void InitUserPage()
        {
            m_userPage = new UserPage(m_mainwindow, m_dbConn, m_user);
            m_userPage.UsersChanged += async () =>
            {
                lstUsers.SelectedItem = null;
                lstUsers.ItemsSource = await LoadUsers(txtSearchUsers.Text);
            };
        }

        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {
            if (m_userPage == null)
            {
                InitUserPage();
            }

            m_userPage.Init(null);
            m_mainwindow.Navigate(m_userPage);
        }

        private void btnUserInfo_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel selitem;
            if ((selitem = lstUsers.SelectedItem as UserViewModel) != null)
            {
                if (m_userPage == null)
                {
                    InitUserPage();
                }

                m_userPage.Init(selitem.Model);
                m_mainwindow.Navigate(m_userPage);
            }
            else
            {
                m_mainwindow.ShowMessage("No User Selected", "Please select a user from the list!");
            }
        }

        private async void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel selitem;
            if ((selitem = lstUsers.SelectedItem as UserViewModel) != null)
            {
                btnDeleteUser.IsEnabled = false;

                try
                {
                    bool allowed = await m_mainwindow.ShowPrompt("Confirmation", string.Format("Are you sure want to delete user {0}?", selitem.UserName));
                    if (!allowed)
                    {
                        return;
                    }

                    if (m_user.Type != "ADMINISTRATOR" && m_user.Type != "MANAGER")
                    {
                        throw new Exception("Insufficient permission");
                    }

                    if (selitem.Model.Equals(m_user))
                    {
                        throw new Exception("Deleting myself is not allowed");
                    }

                    // Deleting user from the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "DELETE FROM users WHERE id = @userId";
                        cmd.Parameters.AddWithValue("userId", selitem.Model.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Refreshing the users list.
                    lstUsers.SelectedItem = null;
                    lstUsers.ItemsSource = await LoadUsers(txtSearchUsers.Text);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete a user.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteUser.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No User Selected", "Please select a user from the list!");
            }
        }

        private void txtSearchUsers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearchUsers_Click(null, null);
            }
        }

        private async void btnSearchUsers_Click(object sender, RoutedEventArgs e)
        {
            lstUsers.SelectedItem = null;
            lstUsers.ItemsSource = await LoadUsers(txtSearchUsers.Text);
        }

        void InitMemberPage()
        {
            m_memberPage = new MemberPage(m_mainwindow, m_dbConn, m_user);
            m_memberPage.MembersChanged += async () =>
            {
                lstMembers.SelectedItem = null;
                lstMembers.ItemsSource = await LoadMembers(txtSearchMembers.Text);
            };
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (m_memberPage == null)
            {
                InitMemberPage();
            }

            m_memberPage.Init(null);
            m_mainwindow.Navigate(m_memberPage);
        }

        private async void btnDeleteMember_Click(object sender, RoutedEventArgs e)
        {
            MemberViewModel selitem;
            if ((selitem = lstMembers.SelectedItem as MemberViewModel) != null)
            {
                btnDeleteMember.IsEnabled = false;

                try
                {
                    bool allowed = await m_mainwindow.ShowPrompt("Confirmation", string.Format("Are you sure want to delete member {0}?", selitem.Model.FirstName));
                    if (!allowed)
                    {
                        return;
                    }

                    if (m_user.Type != "ADMINISTRATOR" && m_user.Type != "MANAGER")
                    {
                        throw new Exception("Insufficient permission");
                    }

                    // Deleting the attached files.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT file_id FROM members_files_link WHERE member_id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", selitem.Model.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                string fileId = reader.GetString(0);

                                using (SQLiteCommand deleteCmd = new SQLiteCommand(m_dbConn))
                                {
                                    deleteCmd.CommandText = "DELETE FROM files WHERE id = @fileId";
                                    deleteCmd.Parameters.AddWithValue("fileId", fileId);
                                    await deleteCmd.ExecuteNonQueryAsync();
                                }

                                try
                                {
                                    AttachedFileAccess.DeleteFile(fileId);
                                }
                                catch
                                {
                                    Console.WriteLine("Unable to delete an attached file.");
                                }
                            }
                        }
                    }

                    // Deleting the notes.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT note_id FROM members_notes_link WHERE member_id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", selitem.Model.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                using (SQLiteCommand deleteCmd = new SQLiteCommand(m_dbConn))
                                {
                                    deleteCmd.CommandText = "DELETE FROM notes WHERE id = @noteId";
                                    deleteCmd.Parameters.AddWithValue("noteId", reader.GetString(0));
                                    await deleteCmd.ExecuteNonQueryAsync();
                                }
                            }
                        }
                    }

                    // Getting the profile image id.
                    string profileImageId = string.Empty;

                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT profile_image_id FROM members WHERE id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", selitem.Model.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                profileImageId = reader.GetString(0);
                            }
                        }
                    }

                    // Deleting selected member from the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "DELETE FROM members WHERE id = @memberId";
                        cmd.Parameters.AddWithValue("memberId", selitem.Model.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }
                    
                    // Deleting profile image.
                    // Should be placed here to prevent foreign key error!
                    try
                    {
                        using (SQLiteCommand deleteCmd = new SQLiteCommand(m_dbConn))
                        {
                            deleteCmd.CommandText = "DELETE FROM files WHERE id = @fileId";
                            deleteCmd.Parameters.AddWithValue("fileId", profileImageId);
                            await deleteCmd.ExecuteNonQueryAsync();
                        }

                        AttachedFileAccess.DeleteFile(profileImageId);
                    }
                    catch
                    {
                        Console.WriteLine("Unable to delete profile image file.");
                    }

                    // Refresing the members list.
                    lstMembers.SelectedItem = null;
                    lstMembers.ItemsSource = await LoadMembers(txtSearchMembers.Text);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected member.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteMember.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No Member Selected", "Please select a member from the list!");
            }
        }

        private void btnMemberInfo_Click(object sender, RoutedEventArgs e)
        {
            MemberViewModel selitem;
            if ((selitem = lstMembers.SelectedItem as MemberViewModel) != null)
            {
                if (m_memberPage == null)
                {
                    InitMemberPage();
                }

                m_memberPage.Init(selitem.Model);
                m_mainwindow.Navigate(m_memberPage);
            }
            else
            {
                m_mainwindow.ShowMessage("No Member Selected", "Please select a member from the list!");
            }
        }

        private void txtSearchMembers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearchMembers_Click(null, null);
            }
        }

        private async void btnSearchMembers_Click(object sender, RoutedEventArgs e)
        {
            lstMembers.SelectedItem = null;
            lstMembers.ItemsSource = await LoadMembers(txtSearchMembers.Text);
        }
    }
}
