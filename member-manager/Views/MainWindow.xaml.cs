﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace member_manager.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public delegate void BackRequestedHandler(MainWindow sender, Page content);
        public event BackRequestedHandler BackRequested;


        public MainWindow()
        {
            InitializeComponent();

            HideBackButton();
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();

            // Go to login page.
            LoginPage loginPage = new LoginPage(this);
            frmMain.Navigate(loginPage);
        }

        public async void ShowMessage(string title, string message)
        {
            await this.ShowMessageAsync(title, message);
        }

        public async Task<bool> ShowPrompt(string title, string message)
        {
            MessageDialogResult dresult = await this.ShowMessageAsync(title, message, MessageDialogStyle.AffirmativeAndNegative);
            return dresult == MessageDialogResult.Affirmative;
        }

        public void Navigate(Page page)
        {
            frmMain.Navigate(page);
        }

        public void GoBack()
        {
            if (frmMain.CanGoBack)
            {
                frmMain.GoBack();
            }
        }

        void ShowBackButton()
        {
            LeftWindowCommands.Width = btnBack.Width;
        }

        void HideBackButton()
        {
            LeftWindowCommands.Width = 0;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            BackRequested?.Invoke(this, frmMain.Content as Page);
        }
        
        private void frmMain_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            if (frmMain.CanGoBack)
            {
                ShowBackButton();
            }
            else
            {
                HideBackButton();
            }
        }
    }
}
