﻿using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace member_manager.Commons
{
    public static class Imaging
    {
        public static async Task<BitmapImage> LoadImageFromStreamAsync(Stream imgStream, int maxDimensions)
        {
            return await Task.Run(() => LoadImageFromStream(imgStream, maxDimensions));
        }

        public static BitmapImage LoadImageFromStream(Stream imgStream, int maxDimensions)
        {
            BitmapImage bmpImage = new BitmapImage();
            bmpImage.BeginInit();
            bmpImage.StreamSource = imgStream;
            bmpImage.CacheOption = BitmapCacheOption.OnLoad;

            // Getting the image size.
            int targetWidth = 0, targetHeight = 0;
            using (Image img = Image.FromStream(imgStream, false, false))
            {
                targetWidth = img.Width;
                targetHeight = img.Height;
            }
            
            // Setting the image size.
            while (targetHeight > maxDimensions || targetWidth > maxDimensions)
            {
                targetWidth /= 2;
                targetHeight /= 2;
            }

            bmpImage.DecodePixelWidth = targetWidth;
            bmpImage.DecodePixelHeight = targetHeight;

            imgStream.Seek(0, SeekOrigin.Begin);
            bmpImage.EndInit();
            bmpImage.Freeze();
            return bmpImage;
        }
    }
}
