﻿using member_manager.Models;

namespace member_manager.ViewModels
{
    public class UserViewModel
    {
        public User Model { get; private set; }

        public string UserName
        {
            get
            {
                return Model.UserName;
            }
        }

        public string FirstName
        {
            get
            {
                return Model.FirstName;
            }
        }

        public string LastName
        {
            get
            {
                return Model.LastName;
            }
        }

        public string Email
        {
            get
            {
                return Model.Email;
            }
        }

        public string Type
        {
            get
            {
                return Model.Type;
            }
        }

        public string LastLoginTimeString
        {
            get
            {
                if (Model.LastLogin.Ticks == 0)
                {
                    return "Never";
                }
                else
                {
                    return Model.LastLogin.ToString();
                }
            }
        }


        public UserViewModel(User model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            UserViewModel comp;
            if ((comp = obj as UserViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
