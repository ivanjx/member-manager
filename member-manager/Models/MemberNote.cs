﻿using System;

namespace member_manager.Models
{
    public class MemberNote
    {
        public string ID { get; set; }

        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public string CreatorUserName { get; set; }


        public override bool Equals(object obj)
        {
            MemberNote comp;
            if ((comp = obj as MemberNote) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
