﻿using member_manager.Models;

namespace member_manager.ViewModels
{
    public class MemberViewModel
    {
        public Member Model { get; private set; }

        public string ID
        {
            get
            {
                return Model.ID;
            }
        }

        public string FirstName
        {
            get
            {
                return Model.FirstName;
            }
        }

        public string LastName
        {
            get
            {
                return Model.LastName;
            }
        }

        public string GenderString
        {
            get
            {
                if (Model.Gender == Gender.Male)
                {
                    return "Male";
                }
                else
                {
                    return "Female";
                }
            }
        }

        public string AddressRegion
        {
            get
            {
                return Model.AddressRegion;
            }
        }

        public string AddressProvince
        {
            get
            {
                return Model.AddressProvince;
            }
        }

        public string MemberSinceDateString
        {
            get
            {
                return Model.MemberSinceDate.ToLongDateString(); // We only need the date.
            }
        }

        public string Status
        {
            get
            {
                return Model.Status;
            }
        }


        public MemberViewModel(Member model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            MemberViewModel comp;
            if ((comp = obj as MemberViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
