﻿-- Notes:
-- ~ Since sqlite does not has a date/time types, I decided to use the INTEGER representation of them.

-- Creating tables
-- user_types table
CREATE TABLE user_types(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- users table
CREATE TABLE users(
	id VARCHAR(32) NOT NULL,
	username VARCHAR(20) NOT NULL UNIQUE,
	password VARCHAR(48) NOT NULL,
	email VARCHAR(254) UNIQUE,
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(40),
	user_type VARCHAR(20) NOT NULL,
	last_login INTEGER,
	PRIMARY KEY(id),
	FOREIGN KEY(user_type) REFERENCES user_types(name) ON DELETE CASCADE
);

-- provinces table
CREATE TABLE provinces(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- regions table
CREATE TABLE regions(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- member_types table
CREATE TABLE member_types(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- member_statuses table
CREATE TABLE member_statuses(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- eye_colors table
CREATE TABLE eye_colors(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- payment_methods table
CREATE TABLE payment_methods(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- files table
CREATE TABLE files(
	id VARCHAR(32) NOT NULL,
	original_name VARCHAR(254) NOT NULL,
	PRIMARY KEY(id)
);

-- members table
CREATE TABLE members(
	id VARCHAR(32) NOT NULL,
	email VARCHAR(254) UNIQUE,
	profile_image_id VARCHAR(32),
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(40),
	father_name VARCHAR(60),
	mother_name VARCHAR(60),
	birth_date INTEGER NOT NULL,
	birth_location VARCHAR(60),
	gender VARCHAR(6) NOT NULL,
	eye_color VARCHAR(20) NOT NULL,
	home_phone VARCHAR(20),
	cell_phone VARCHAR(20),
	work_phone VARCHAR(20),
	address_street VARCHAR(30),
	address_city VARCHAR(20),
	address_local VARCHAR(20),
	address_region VARCHAR(20),
	address_province VARCHAR(20),
	address_postal VARCHAR(10),
	member_since_date INTEGER NOT NULL,
	member_status VARCHAR(20) NOT NULL,
	member_type VARCHAR(20) NOT NULL,
	payment_method VARCHAR(20) NOT NULL,
	is_canadian INT(1) NOT NULL, -- 0 for no, otherwise yes.
	data_added_date INTEGER NOT NULL,
	data_modified_date INTEGER NOT NULL,
	data_adder_id VARCHAR(32) NOT NULL,
	data_modifier_id VARCHAR(32) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(profile_image_id) REFERENCES files(id),
	FOREIGN KEY(eye_color) REFERENCES eye_colors(name),
	FOREIGN KEY(address_region) REFERENCES regions(name) ON DELETE CASCADE,
	FOREIGN KEY(address_province) REFERENCES provinces(name) ON DELETE CASCADE,
	FOREIGN KEY(member_status) REFERENCES member_statuses(name) ON DELETE CASCADE,
	FOREIGN KEY(member_type) REFERENCES member_types(name) ON DELETE CASCADE,
	FOREIGN KEY(payment_method) REFERENCES payment_methods(name) ON DELETE CASCADE,
	FOREIGN KEY(data_adder_id) REFERENCES users(id),
	FOREIGN KEY(data_modifier_id) REFERENCES users(id)
);

-- file_types table
CREATE TABLE file_types(
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(name)
);

-- members_files_link table
CREATE TABLE members_files_link(
	file_id VARCHAR(32) NOT NULL,
	member_id VARCHAR(32) NOT NULL,
	type VARCHAR(20) NOT NULL,
	added_time INTEGER NOT NULL,
	last_modified_time INTEGER NOT NULL,
	adder_id VARCHAR(32) NOT NULL,
	modifier_id VARCHAR(32) NOT NULL,
	FOREIGN KEY(file_id) REFERENCES files(id) ON DELETE CASCADE,
	FOREIGN KEY(member_id) REFERENCES members(id) ON DELETE CASCADE,
	FOREIGN KEY(type) REFERENCES file_types(name) ON DELETE CASCADE
	FOREIGN KEY(adder_id) REFERENCES users(id),
	FOREIGN KEY(modifier_id) REFERENCES users(id)
);

-- notes table.
CREATE TABLE notes(
	id VARCHAR(32) NOT NULL,
	content TEXT NOT NULL,
	creator_id VARCHAR(32) NOT NULL,
	date_created INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(creator_id) REFERENCES users(id) ON DELETE CASCADE
);

-- members_notes_link table.
CREATE TABLE members_notes_link(
	member_id VARCHAR(32) NOT NULL,
	note_id VARCHAR(32) NOT NULL,
	FOREIGN KEY(member_id) REFERENCES members(id) ON DELETE CASCADE,
	FOREIGN KEY(note_id) REFERENCES notes(id) ON DELETE CASCADE
);


-- Filling the tables with data.
-- Filling table user_types.
INSERT INTO user_types VALUES('VIEWER');
INSERT INTO user_types VALUES('MANAGER');
INSERT INTO user_types VALUES('ADMINISTRATOR');

-- Filling table member_types.
INSERT INTO member_types VALUES('INDIVIDUAL');
INSERT INTO member_types VALUES('CHILDREN');

-- Filling table member_statuses.
INSERT INTO member_statuses VALUES('ACCEPTED');
INSERT INTO member_statuses VALUES('WITHDRAWN');
INSERT INTO member_statuses VALUES('PROCESSING');
INSERT INTO member_statuses VALUES('CONFIRMED');
INSERT INTO member_statuses VALUES('REJECTED');

-- Filling table eye_colors.
INSERT INTO eye_colors VALUES('BLACK');
INSERT INTO eye_colors VALUES('BLUE');
INSERT INTO eye_colors VALUES('BROWN');
INSERT INTO eye_colors VALUES('GRAY');
INSERT INTO eye_colors VALUES('GREEN');
INSERT INTO eye_colors VALUES('HAZEL');
INSERT INTO eye_colors VALUES('MAROON');
INSERT INTO eye_colors VALUES('PINK');
INSERT INTO eye_colors VALUES('MULTICOLORED');
INSERT INTO eye_colors VALUES('OTHER');

-- Filling table payment_methods.
INSERT INTO payment_methods VALUES('CASH');

-- Filling table file_types.
INSERT INTO file_types VALUES('GENERAL');
INSERT INTO file_types VALUES('GENEOLOGY');
INSERT INTO file_types VALUES('IDENTIFICATION');
INSERT INTO file_types VALUES('SUPPORTING DOCUMENTS');
INSERT INTO file_types VALUES('APPLICATION');
INSERT INTO file_types VALUES('PAYMENT');

-- Filling table provinces.
INSERT INTO provinces VALUES('ONTARIO');
INSERT INTO provinces VALUES('QUEBEC');
INSERT INTO provinces VALUES('NOVA SCOTIA');
INSERT INTO provinces VALUES('NEW BRUNSWICK');
INSERT INTO provinces VALUES('MANITOBA');
INSERT INTO provinces VALUES('BRITISH COLUMBIA');
INSERT INTO provinces VALUES('PRINCE EDWARD ISLAND');
INSERT INTO provinces VALUES('SASKATCHEWAN');
INSERT INTO provinces VALUES('ALBERTA');
INSERT INTO provinces VALUES('NEWFOUNDLAND AND LABRADOR');

-- Filling table regions.
INSERT INTO regions VALUES('NORTHWEST');
INSERT INTO regions VALUES('INTERLAKE');
INSERT INTO regions VALUES('SOUTHEAST');
INSERT INTO regions VALUES('SOUTHWEST');
INSERT INTO regions VALUES('THE PAS');
INSERT INTO regions VALUES('THOMPSON');
INSERT INTO regions VALUES('WINNIPEG');

