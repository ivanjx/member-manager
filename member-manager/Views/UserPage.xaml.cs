﻿using member_manager.Commons;
using member_manager.Models;
using System;
using System.Data.Common;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Controls;

namespace member_manager.Views
{
    /// <summary>
    /// Interaction logic for UserPage.xaml
    /// </summary>
    public partial class UserPage : Page
    {
        public delegate void UsersChangedHandler();
        public event UsersChangedHandler UsersChanged;

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        User m_user;
        User m_currentUser;


        public UserPage(MainWindow mainwindow, SQLiteConnection dbConn, User currentUser)
        {
            InitializeComponent();

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (sender, page) =>
            {
                if (page != null && page == this)
                {
                    ClearInputs();
                    m_mainwindow.GoBack();
                }
            };

            m_dbConn = dbConn;
            m_currentUser = currentUser;
            LoadUserTypes();
        }

        public void Init(User user)
        {
            ClearInputs();
            m_user = user;

            if (user != null)
            {
                lblTitle.Text = "User Info";
                txtFirstName.Text = user.FirstName;
                txtLastName.Text = user.LastName;
                txtUsername.Text = user.UserName;
                txtPassword.Password = user.Password;
                txtEmail.Text = user.Email;
                cmbUserType.SelectedItem = user.Type;
            }
            else
            {
                lblTitle.Text = "Add New User";
            }
        }

        void ClearInputs()
        {
            txtFirstName.Clear();
            txtLastName.Clear();
            txtUsername.Clear();
            txtEmail.Clear();
            txtPassword.Clear();
            cmbUserType.SelectedItem = null;
        }

        async void LoadUserTypes()
        {
            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT name FROM user_types";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        cmbUserType.Items.Add(reader.GetString(0));
                    }
                }
            }
        }

        private async void btnOK_Click(object sender, RoutedEventArgs e)
        {
            btnOK.IsEnabled = false;

            try
            {
                // Checking user rights.
                if (m_currentUser.Type != "ADMINISTRATOR" && m_currentUser.Type != "MANAGER")
                {
                    throw new Exception("Insufficient permission");
                }

                // Checking user inputs.
                if (string.IsNullOrEmpty(txtFirstName.Text))
                {
                    throw new Exception("First name is null");
                }

                if (string.IsNullOrEmpty(txtUsername.Text))
                {
                    throw new Exception("Username is null");
                }

                if (string.IsNullOrEmpty(txtPassword.Password))
                {
                    throw new Exception("Password is null");
                }

                if (cmbUserType.SelectedItem == null)
                {
                    throw new Exception("User type is not selected");
                }

                // Applying changes.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    if (m_user == null) // Add new user.
                    {
                        cmd.CommandText = "INSERT INTO users(id, username, password, first_name, last_name, email, user_type, last_login) VALUES(@userId, @username, @password, @firstName, @lastName, @email, @userType, 0)";
                        cmd.Parameters.AddWithValue("userId", await Randomizer.RandomStringAsync(32));
                    }
                    else // Update existing user.
                    {
                        cmd.CommandText = "UPDATE users SET username = @username, password = @password, first_name = @firstName, last_name = @lastName, email = @email, user_type = @userType WHERE id = @userId";
                        cmd.Parameters.AddWithValue("userId", m_user.ID);
                    }


                    cmd.Parameters.AddWithValue("userType", cmbUserType.SelectedItem as string);
                    cmd.Parameters.AddWithValue("username", txtUsername.Text);
                    cmd.Parameters.AddWithValue("password", txtPassword.Password);
                    cmd.Parameters.AddWithValue("firstName", txtFirstName.Text);
                    cmd.Parameters.AddWithValue("lastName", txtLastName.Text);
                    cmd.Parameters.AddWithValue("email", txtEmail.Text);
                    await cmd.ExecuteNonQueryAsync();
                }

                UsersChanged?.Invoke();
                ClearInputs();
                m_mainwindow.GoBack();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK.IsEnabled = true;
            }
        }
    }
}
