﻿using System;

namespace member_manager.Models
{
    public class User
    {
        public string ID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Type { get; set; }

        public DateTime LastLogin { get; set; }


        public override bool Equals(object obj)
        {
            User comp;
            if ((comp = obj as User) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
