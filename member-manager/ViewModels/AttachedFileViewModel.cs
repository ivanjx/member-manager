﻿using member_manager.Models;

namespace member_manager.ViewModels
{
    public class AttachedFileViewModel
    {
        public AttachedFile Model { get; private set; }

        public string OriginalFileName
        {
            get
            {
                return Model.OriginalFileName;
            }
        }

        public string Type
        {
            get
            {
                return Model.Type;
            }
        }

        public string FriendlySizeString
        {
            get
            {
                (string unitName, float size) friendlySize = GetFriendlySize(Model.Size);
                return string.Format("{0} {1}", friendlySize.size.ToString("F1"), friendlySize.unitName);
            }
        }

        public string AddedTimeString
        {
            get
            {
                return Model.AddedTime.ToString();
            }
        }

        public string AdderUserName
        {
            get
            {
                return Model.AdderUserName;
            }
        }

        public string LastModifiedTimeString
        {
            get
            {
                return Model.LastModifiedTime.ToString();
            }
        }

        public string LastModifierUserName
        {
            get
            {
                return Model.LastModifierUserName;
            }
        }


        public AttachedFileViewModel(AttachedFile model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            AttachedFileViewModel comp;
            if ((comp = obj as AttachedFileViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static (string unit, float size) GetFriendlySize(long sizeInByte)
        {
            float finalSize = 0;
            string unitName = string.Empty;

            if(sizeInByte < 1024)
            {
                unitName = "B";
                finalSize = sizeInByte;
            }
            else if (sizeInByte < 1024 * 1024)
            {
                unitName = "KB";
                finalSize = sizeInByte / 1024;
            }
            else if (sizeInByte < 1024 * 1024 * 1024)
            {
                unitName = "MB";
                finalSize = sizeInByte / (1024 * 1024);
            }
            else
            {
                unitName = "GB";
                finalSize = sizeInByte / (1024 * 1024 * 1024);
            }

            return (unit: unitName, size: finalSize);
        }
    }
}
