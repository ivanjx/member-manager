﻿using System;

namespace member_manager.Models
{
    public class AttachedFile
    {
        public string ID { get; set; }

        public string OriginalFileName { get; set; }

        public string OriginalPath { get; set; }

        public long Size { get; set; }

        public string Type { get; set; }

        public string AdderUserName { get; set; }

        public DateTime AddedTime { get; set; }

        public string LastModifierUserName { get; set; }

        public DateTime LastModifiedTime { get; set; }


        public override bool Equals(object obj)
        {
            AttachedFile comp;
            if ((comp = obj as AttachedFile) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
