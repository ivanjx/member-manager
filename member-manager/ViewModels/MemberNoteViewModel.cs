﻿using member_manager.Models;

namespace member_manager.ViewModels
{
    public class MemberNoteViewModel
    {
        public MemberNote Model { get; private set; }

        public string Content
        {
            get
            {
                return Model.Content;
            }
        }

        public string CreatorUserName
        {
            get
            {
                return Model.CreatorUserName;
            }
        }

        public string DateCreatedString
        {
            get
            {
                return Model.DateCreated.ToString();
            }
        }


        public MemberNoteViewModel(MemberNote model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            MemberNoteViewModel comp;
            if ((comp = obj as MemberNoteViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
